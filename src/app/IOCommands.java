package app;

//#region Import
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
//#endregion

/**
 * IOCommands
 */
public class IOCommands {

    //#region Attributes
    private BufferedReader mScreenReading;
    private PrintStream mScreenWriting;

    private BufferedReader mNetworkReading;
    private PrintStream mNetworkWriting;
    //#endregion

    //#region Constructor

    /**
     * Class constructor IOCommands
     */
    public IOCommands(Socket pSocket){
        this.mScreenReading = new BufferedReader(new InputStreamReader(System.in));
        this.mScreenWriting = System.out; // Because `System.out` is already a `Printstream`
        try {
            this.mNetworkReading = new BufferedReader(new InputStreamReader(pSocket.getInputStream()));
            this.mNetworkWriting = new PrintStream(pSocket.getOutputStream());  
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //#endregion

    //#region Method
    
    /**
     * This method write in terminal
     * @param pText line to print on the terminal
     */
    public void screenWriting(String pText) throws IllegalArgumentException{
        if (pText == null) 
            throw new IllegalArgumentException("The argument cannot be null");
        this.mScreenWriting.println(pText);
    }

    /**
     * this method read a line in terminal
     * @return line insert in terminal
     */
    public String screenReading(){
        String vText = null;
        try {
            vText = this.mScreenReading.readLine(); 
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vText;
    }

    /**
     * This method write into network
     * @param pText to write on the network
     */
    public void networkWriting(String pText) throws IllegalArgumentException{
        if (pText == null) 
            throw new IllegalArgumentException("The argument cannot be null");
        this.mNetworkWriting.println(pText);
    }

    /**
     * this method receive a network line
     * @return line receive by network
     */
    public String networkReading(){
        String vText = null;
        try {
            vText = this.mNetworkReading.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vText;
    }
    //#endregion
}