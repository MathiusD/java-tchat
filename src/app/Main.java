package app;

//#region Import
import app.IOCommands;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
//#endregion

/**
 * Main
 */
public class Main {

    /**
     * Main execution of `Main`
     * @param args
     */
    public static void main(final String[] args) {
        IOCommands vIOCommands = null;
        try {
            vIOCommands = new IOCommands(new Socket("172.16.1.64", 5999));
        } catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        String vText = "";
        String vOutput = "quit";
        Boolean vTchat = true;
        while (vTchat == true) {
            vText = vIOCommands.screenReading();
            if (vText.toLowerCase().trim().equals(vOutput))
            {
                vTchat = false;
            }
            else
            {
                try {
                    vIOCommands.networkWriting(vText);   
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                vText = vIOCommands.networkReading();
                try {
                    vIOCommands.screenWriting(vText); 
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}