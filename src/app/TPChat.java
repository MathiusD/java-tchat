package app;

/**
 * TPChat
 */
public class TPChat {

    /**
     * Main execution `TPChat`
     * @param args
     */
    public static void main(String[] args) {
        ClientChat vClient = new ClientChat();
        vClient.Login();
        vClient.ConsoleDialog();
    }
}