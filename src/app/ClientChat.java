package app;

//#region Import
import java.net.Socket;
import java.io.IOException;
import java.net.UnknownHostException;
//#endregion

/**
 * ClientChat
 */
public class ClientChat {

    //#region Attributes
    private IOCommands mIOCommands;
    private String mUser;
    //#endregion
    
    //#region Constructor
    public ClientChat(){
        try {
            this.mIOCommands = new IOCommands(new Socket("172.16.1.64", 6666));
        } catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    //#endregion 

    //#region Method
    /**
     * Treatment client / server exchanges
     */
    public void ConsoleDialog(){
        Integer vChoice = 0;
        do{
            this.mIOCommands.screenWriting("Que tu souhaite-tu faire ?\n");
            this.mIOCommands.screenWriting("1 -> Voir la liste des utilisateurs connectés\n");
            this.mIOCommands.screenWriting("2 -> Relever tes messages\n");
            this.mIOCommands.screenWriting("3 -> Envoyer un message à tous\n");
            this.mIOCommands.screenWriting("4 -> Envoyer un message privé\n");
            this.mIOCommands.screenWriting("5 -> Quitter\n");
            this.mIOCommands.screenWriting("Taper votre choix >");
            String vReturn = this.mIOCommands.screenReading();
            try
            {
                vChoice = Integer.parseInt(vReturn);
            }
            catch (NumberFormatException Exec)
            {
                this.mIOCommands.screenWriting("Veuillez Saisir un chiffre\n");
            }
            switch (vChoice)
            {
                case 1:
                    this.Users(true);
                    break;

                case 2:
                    this.Relieve();
                    break;

                case 3:
                    this.Send();
                    break;

                case 4:
                    this.SendPM();
                    break;

                case 5:
                    this.Quit();
                    this.mIOCommands.screenWriting("Au revoir " + mUser + ", tu es déconnecté.\n");
                    break;

                case 0:
                    break;
            }
        }while (vChoice != 5);

    }

    public void Login(){
        String vLoginUser = null;
        String vNetworkResponse = null;
        this.mIOCommands.networkReading();
        do{
            this.mIOCommands.screenWriting("Entrez votre login : ");
            vLoginUser = this.mIOCommands.screenReading();
            this.mIOCommands.networkWriting("USER:<" + vLoginUser.trim() + ">");
            vNetworkResponse = this.mIOCommands.networkReading();
            this.mIOCommands.screenWriting(vNetworkResponse);
        }while(!CheckValidity(vNetworkResponse));
        this.mUser = vLoginUser;
        this.mIOCommands.screenWriting("Bonjour " + mUser + ".");
    }
    
    private String Users(Boolean pIHM){
        this.mIOCommands.networkWriting("USERS");
        String vNetworkResponse = mIOCommands.networkReading();
        String vData = this.GetAllMsg(vNetworkResponse);
        String[] vTab = vData.split("\n");
        int vLength = vTab.length - 1;
        String vAff;
        Boolean vFirst = true;
        char vChar = ' ';
        char vFChar;
        if (pIHM == false)
        {
            vAff = "";
            vFChar = ';';
        }
        else
        {
            vAff = "Les utilisateurs connectés sont :";
            vFChar = ',';
        }
        for (String vName : vTab)
        {
            if (vFirst == false)
            {
                vAff += vFChar;
            }
            if (pIHM == true)
            {
                vAff += vChar;
            }
            vAff += vName;
            if (vFirst == true)
            {
               vFirst = false;
            }
        }
        if (pIHM == true)
        {
            this.mIOCommands.screenWriting(vAff);
        }
        return vAff;
    }

    private void Relieve(){
        mIOCommands.networkWriting("MESSAGES");
        String vNetworkResponse = mIOCommands.networkReading(); 
        String[] vTab = GetAllMsg(vNetworkResponse).split("\n");
        int vLength = vTab.length;
        if (vLength > 0)
        {
            this.mIOCommands.screenWriting("Vous avez reçu " + vLength + " message(s) :\n");
            for (String vMsg : vTab)
            {
                if (!(vMsg.equals("")))
                {
                    String vAff = "Message ";
                    String[] vMsgHead = vMsg.split(":")[0].split(">");
                    if (!(vMsgHead[1].equals("ALL")))
                    {
                        vAff += "privé ";
                    }
                    vAff += "de " + vMsgHead[0] + " : " ;
                    vAff += vMsg.substring(vMsg.split(":")[0].length() + 1);
                    this.mIOCommands.screenWriting(vAff);
                }
            }
        }
    }

    private void Send(){
        mIOCommands.screenWriting("Tape ton message :");
        String vMsg = mIOCommands.screenReading();
        mIOCommands.networkWriting("SEND:"+ vMsg);
        String vNetworkResponse = mIOCommands.networkReading();
        if(CheckValidity(vNetworkResponse)){
            mIOCommands.screenWriting("Ton message a bien ete envoye");
        }else{
            mIOCommands.screenWriting("Ton message n'a pas pu etre envoye");
        }
    }

    private void SendPM(){
        Boolean vFalse = true;
        String vUser = null;
        do
        {
            mIOCommands.screenWriting("A qui souhaites-tu envoyer ton message ?");
            vUser = mIOCommands.screenReading();
            String[] vTab = this.Users(false).split(";");
            for (String vName: vTab)
            {
                if (vName.equals(vUser))
                {
                    vFalse = false;
                }
            }
            if (vFalse == true)
            {
                this.mIOCommands.screenWriting("Utilisateur Incorrect.\n");
            }
        } while(vFalse);
        mIOCommands.screenWriting("Tape ton message :");
        String vMsg = mIOCommands.screenReading();
        mIOCommands.networkWriting("SEND " + vUser +":"+vMsg);
        String vNetworkResponse = mIOCommands.networkReading();
        if(CheckValidity(vNetworkResponse)){
            mIOCommands.screenWriting("Ton message a bien ete envoye\n");
        }else{
            mIOCommands.screenWriting("Ton message n'a pas pu etre envoye\n");
        }
    }

    private void Quit(){
        this.mIOCommands.networkWriting("QUIT");
    }

    private Boolean CheckValidity(String pNetworkResponse){
        Boolean vError = true;
        if (pNetworkResponse.startsWith("Err:")){
            vError = false;
        }
        return vError;
    }

    private String GetAllMsg(String pNetworkResponse){
        String vReturn = null;
        if(CheckValidity(pNetworkResponse)){
            vReturn = "";
            String vRes = pNetworkResponse.substring(3);
			if(vRes.charAt(0) >= 48 && vRes.charAt(0) <= 57) {
				String vNumTxt = "";
                for (int vIndex = 0; vRes.charAt(vIndex) != ' ' ; vIndex++) {
                    vNumTxt+=vRes.charAt(vIndex); 
                }
                int vNum = Integer.parseInt(vNumTxt);
				for(int vIndex = 0; vIndex < vNum; vIndex++) {
                    vReturn += mIOCommands.networkReading() +  "\n";
				}
			}
        }
        return vReturn;
    }
    //#endregion
}