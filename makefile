default_target: tpchat

compile:
	@echo -ne "Compiling...\n"
	@javac -d . src/app/*

build: compile
	@echo -ne "Clean...\n"
	@jar cf tchat.jar app/*.class
	@rm -rf app

launch: build
	@echo -ne "Launch...\n"

main: launch
	@java -cp tchat.jar app.Main

tpchat: launch
	@java -cp tchat.jar app.TPChat